#!/bin/bash

find . -name '*.pyc' -delete
pytest tests
gunicorn things.app
