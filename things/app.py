import falcon

from .images import ImagesResource
from .things import ThingsResource

api = application = falcon.API()

index = ThingsResource()
images = ImagesResource()

api.add_route('/', index)
api.add_route('/images', images)
