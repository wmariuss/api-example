import json
import falcon


class ThingsResource(object):
    def on_get(self, req, resp):
        doc = {
            'url': '/images'
        }

        resp.body = json.dumps(doc, ensure_ascii=False)
        resp.status = falcon.HTTP_200
